cmake_minimum_required(VERSION 3.27.8)

set(TARGET_NAME h86)

# set the project name and version
project(${TARGET_NAME} VERSION 0.1)

# specify the C++ standard
set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED True)

set(CMAKE_ARCHIVE_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/lib)
set(CMAKE_LIBRARY_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/lib)
set(CMAKE_RUNTIME_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/bin)

# generate compilation_commands.json
set(CMAKE_EXPORT_COMPILE_COMMANDS On)

set(source_dir ${CMAKE_CURRENT_SOURCE_DIR}/src)
FILE(GLOB my_sources ${source_dir}/*.cpp)

# add the executable
add_executable(${TARGET_NAME} ${my_sources})

# enable all warnings
if (MSVC)
    target_compile_options(${TARGET_NAME} PRIVATE /W4 /WX)
else ()
    target_compile_options(${TARGET_NAME} PRIVATE -Wall -Wextra -Wpedantic -Werror)
endif ()

