#pragma once

#include <cstddef>
#include <stdint.h>

// ----------------------------------------------

typedef uint8_t     u8;
typedef uint16_t    u16;
typedef uint32_t    u32;
typedef uint64_t    u64;

typedef int8_t      i8;
typedef int16_t     i16;
typedef int32_t     i32;
typedef int64_t     i64;

typedef size_t      usize;

typedef int32_t     b32;

// ----------------------------------------------

#define ARRAY_COUNT(arr) (sizeof(arr) / sizeof(arr)[0])

// ----------------------------------------------

namespace h86 {

// ----------------------------------------------

// enum class operation_type {
//     opnone,
//
// #define INST(mnemonic, ...) op_##mnemonic
// #define INSTALT()
// // #include "instr_table.inl"
//
//     opcount
// };

enum class operation_type : u32
{
    op_none,

    op_mov,

    op_push,

    op_pop,

    op_xchg,

    op_in,

    op_out,

    op_xlat,
    op_lea,
    op_lds,
    op_les,
    op_lahf,
    op_sahf,
    op_pushf,
    op_popf,

    op_add,

    op_adc,

    op_inc,

    op_aaa,
    op_daa,

    op_sub,

    op_sbb,

    op_dec,

    op_neg,

    op_cmp,

    op_aas,
    op_das,
    op_mul,
    op_imul,
    op_aam,
    op_div,
    op_idiv,
    op_aad,
    op_cbw,
    op_cwd,

    op_not,
    op_shl,
    op_shr,
    op_sar,
    op_rol,
    op_ror,
    op_rcl,
    op_rcr,

    op_and,

    op_test,

    op_or,

    op_xor,

    op_rep,
    op_movs,
    op_cmps,
    op_scas,
    op_lods,
    op_stos,

    op_call,

    op_jmp,

    op_ret,

    op_retf,

    op_je,
    op_jl,
    op_jle,
    op_jb,
    op_jbe,
    op_jp,
    op_jo,
    op_js,
    op_jne,
    op_jnl,
    op_jg,
    op_jnb,
    op_ja,
    op_jnp,
    op_jno,
    op_jns,
    op_loop,
    op_loopz,
    op_loopnz,
    op_jcxz,

    op_int,
    op_int3,

    op_into,
    op_iret,

    op_clc,
    op_cmc,
    op_stc,
    op_cld,
    op_std,
    op_cli,
    op_sti,
    op_hlt,
    op_wait,
    op_esc,
    op_lock,
    op_segment,

    _count,
};

// ----------------------------------------------

enum class instr_flag {
    lock    = (1 << 0),
    rep     = (1 << 1),
    segment = (1 << 2),
    wide    = (1 << 3),
};

// ----------------------------------------------

enum class register_index : u8 {
    none,

    a,
    b,
    c,
    d,
    sp,
    bp,
    si,
    di,
    es,
    cs,
    ss,
    ds,
    ip,
    flags,

    _count,
};

// ----------------------------------------------

enum class effective_addr_base {
    direct,
    bx_si,
    bx_di,
    bp_si,
    bp_di,
    si,
    di,
    bp,
    bx,

    _count
};

// ----------------------------------------------

struct effective_addr_expr {
    register_index          segment;
    effective_addr_base     base;
    i32                     displacement;   // 8 or 16 bit
};

// ----------------------------------------------

struct register_access {
    register_index  index;
    u8              offset;     // "high bits = offset 1"
    u8              count;
};

// ----------------------------------------------

// TODO(lm): use option instead of none
enum class operand_type {
    none,
    reg,
    memory,
    immediate,
    relative_immediate,
};

// ----------------------------------------------

struct instr_operand {
    operand_type type;
    union {
        effective_addr_expr address;
        register_access     reg;
        u32                 immediate_u32;
        i32                 immediate_i32;
    };
};

// ----------------------------------------------

struct instr {
    u32             address;    // address where the operation was decoded
    u32             size;
    operation_type  op;
    u32             flags;

    instr_operand operands[2];
};

// ----------------------------------------------

}

