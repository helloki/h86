#ifndef INST
#define INST(Mnemonic, Encoding, ...) {Op_##Mnemonic, Encoding, __VA_ARGS__},
#endif

#ifndef INSTALT
#define INSTALT INST
#endif

#define B(bits) {Instr_Bit_Usage::literal, sizeof(#bits)-1, 0, 0b##bits}
#define D {Instr_Bit_Usage::d, 1}
#define S {Instr_Bit_Usage::s, 1}
#define W {Instr_Bit_Usage::w, 1}
#define V {Instr_Bit_Usage::v, 1}
#define Z {Instr_Bit_Usage::z, 1}

#define XXX {Instr_Bit_Usage::data, 3, 0}
#define YYY {Instr_Bit_Usage::data, 3, 3}

#define RM  {Instr_Bit_Usage::rm,  3}
#define MOD {Instr_Bit_Usage::mod, 3}
#define REG {Instr_Bit_Usage::reg, 3}
#define SR  {Instr_Bit_Usage::sr,  2}

#define IMP_W(val)   {Instr_Bit_Usage::w,   0, 0, val}
#define IMP_REG(val) {Instr_Bit_Usage::reg, 0, 0, val}
#define IMP_RM(val)  {Instr_Bit_Usage::rm,  0, 0, val}
#define IMP_D(val)   {Instr_Bit_Usage::d,   0, 0, val}
#define IMP_S(val)   {Instr_Bit_Usage::s,   0, 0, val}

#define DISP        { Instr_Bit_Usage::has_disp,       0, 0, 1}
#define ADDR        { Instr_Bit_Usage::has_disp,       0, 0, 1}, { Instr_Bit_Usage::disp_always_w, 0, 0, 1}
#define DATA        { Instr_Bit_Usage::has_data,       0, 0, 1}
#define DATA_IF_W   { Instr_Bit_Usage::w_makes_data_w, 0, 0, 1}
#define FLAGS(f)    { f, 0, 0, 1}

// ----------------------------------------------

INST(mov, {B(100010), D, W, MOD, REG, RM})
INSTALT(mov, {B(1100011), W, MOD, B(000), RM, DATA, DATA_IF_W, ImpD(0)})
INSTALT(mov, {B(1011), W, REG, DATA, DATA_IF_W, IMP_D(1)})
INSTALT(mov, {B(1010000), W, ADDR, IMP_REG(0), IMP_MOD(0), IMP_RM(0b110), IMP_D(1)})
INSTALT(mov, {B(1010001), W, ADDR, IMP_REG(0), IMP_MOD(0), IMP_RM(0b110), IMP_D(0)})
INSTALT(mov, {B(100011), D, B(0), MOD, B(0), SR, RM}) // NOTE(casey): This collapses 2 entries in the 8086 table by adding an explicit D bit

// ----------------------------------------------

#undef INST
#undef INSTALT

#undef B
#undef D
#undef S
#undef W
#undef V
#undef Z

#undef XXX
#undef YYY

#undef RM
#undef MOD
#undef REG
#undef SR

#undef IMP_W
#undef IMP_REG
#undef IMP_RM
#undef IMP_D
#undef IMP_S

#undef DISP
#undef ADDR
#undef DATA
#undef DATA_IF_W
#undef FLAGS
