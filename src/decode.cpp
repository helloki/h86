#include <cassert>
#include <iostream>

#include "decode.h"
#include "common.h"
#include "memory.h"

namespace h86 {

// TODO
instr_format instr_formats[] = {
// #include "instr_table.inl"
    {}
};

disasm_context default_disasm_context()
{
    disasm_context result = {};
    result.default_segment = register_index::ds;
    return result;
}

static instr_operand get_reg_operand(u32 intel_reg_idx, b32 wide)
{
    register_access tbl[][2] = {
        {{register_index::a, 0, 1}, {register_index::a, 0, 2}},
        {{register_index::c, 0, 1}, {register_index::c, 0, 2}},
        {{register_index::d, 0, 1}, {register_index::d, 0, 2}},
        {{register_index::b, 0, 1}, {register_index::b, 0, 2}},
        {{register_index::a, 1, 1}, {register_index::sp, 0, 2}},
        {{register_index::c, 1, 1}, {register_index::bp, 0, 2}},
        {{register_index::d, 1, 1}, {register_index::si, 0, 2}},
        {{register_index::b, 1, 1}, {register_index::di, 0, 2}},
    };

    instr_operand result = {};
    result.type = operand_type::reg;
    result.reg = tbl[intel_reg_idx & 0x7][wide != 0];
    return result;
}

static u32 parse_data_value(memory* mem, segmented_access* access, b32 exists,
                            b32 wide, b32 sign_extended)
{
    u32 result = {};

    if (exists) {
        if (wide) {
            u8 d0 = read_memory(mem, absolute_address_of(*access, 0));
            u8 d1 = read_memory(mem, absolute_address_of(*access, 1));
            result = (d1 << 8) | d0;
            access->segment_offset += 2;
        } else {
            result = read_memory(mem, absolute_address_of(*access));
            if (sign_extended) {
                result = (i32)*(i8*)&result;
            }
            access->segment_offset += 1;
        }
    }

    return result;
}

static instr try_decode(disasm_context* ctx, instr_format* fmt, memory* mem,
                        segmented_access at)
{
    instr dest = {};
    u32 has_bits = 0;
    u32 bits[static_cast<usize>(bit_usage::_count)] = {};
    b32 valid = true;

    u32 starting_address = absolute_address_of(at);

    u8 bits_pending_count = 0;
    u8 bits_pending = 0;

    const auto bits_at = [&bits](bit_usage usage) -> u32 {
        return bits[static_cast<usize>(usage)];
    };

    for (u32 i = 0; valid && (i < ARRAY_COUNT(fmt->bits)); ++i) {
        instr_bits test_bits = fmt->bits[i];

        if (test_bits.usage == bit_usage::literal && test_bits.bit_count == 0) {
            // NOTE(lm): that's the end of the instruction format
            break;
        }

        // NOTE(lm): initialize with potentially existing, literal value
        u32 read_bits = test_bits.value;
        if (test_bits.bit_count != 0) {
            if (bits_pending_count == 0) {
                bits_pending_count = 8;
                bits_pending = read_memory(mem, absolute_address_of(at));
                ++at.segment_offset;
            }

            // NOTE(lm): bit values don't straddle byte boundaries
            assert(test_bits.bit_count <= bits_pending_count);

            bits_pending_count -= test_bits.bit_count;
            read_bits = bits_pending;
            read_bits >>= bits_pending_count;
            read_bits &= ~(0xff << test_bits.bit_count);
        }

        if (test_bits.usage == bit_usage::literal) {
            valid = valid && (read_bits == test_bits.value);
        } else {
            bits[bits_at(test_bits.usage)] |=
                (read_bits << test_bits.shift);
            has_bits |= (1 << bits_at(test_bits.usage));
        }
    }

    if (valid) {
        u32 mod = bits[bits_at(bit_usage::mod)];
        u32 rm  = bits[bits_at(bit_usage::rm)];
        u32 w   = bits[bits_at(bit_usage::w)];
        u32 s   = bits[bits_at(bit_usage::s)];
        u32 d   = bits[bits_at(bit_usage::d)];

        b32 has_direct_address = ((mod == 0b00) && (rm == 0b110));
        b32 has_displacement = (
            (bits[bits_at(bit_usage::has_disp)]) ||
            (mod == 0b10) || (mod == 0b01) ||
            (has_direct_address)
        );
        b32 displacement_is_w = (
            (bits[bits_at(bit_usage::disp_always_w)]) ||
            (mod == 0b10) ||
            (has_direct_address)
        );
        b32 data_is_w = (
            (bits[bits_at(bit_usage::w_makes_data_w)]) &&
            !s &&
            w
        );
        b32 has_data = bits[bits_at(bit_usage::has_data)];

        bits[bits_at(bit_usage::disp)] |= parse_data_value(
            mem, &at, has_displacement, displacement_is_w, (!displacement_is_w));
        bits[bits_at(bit_usage::data)] |= parse_data_value(
            mem, &at, has_data, data_is_w, s);

        dest.op = fmt->op;
        dest.flags = ctx->additional_flags;
        dest.address = starting_address;
        dest.size = absolute_address_of(at) - starting_address;

        if (w) {
            dest.flags |= static_cast<u32>(instr_flag::wide);
        }

        u32 disp = bits[bits_at(bit_usage::disp)];
        i16 displacement = (i16)disp;

        instr_operand* reg_operand = &dest.operands[d ? 0 : 1];
        instr_operand* mod_operand = &dest.operands[d ? 1 : 0];

        if (has_bits & (1 << bits_at(bit_usage::sr))) {
            reg_operand->type = operand_type::reg;
            reg_operand->reg.index = static_cast<register_index>(
                static_cast<u32>(register_index::es) +
                (bits[static_cast<usize>(bit_usage::sr)] & 0x3));
            reg_operand->reg.count = 2;
        }

        if (has_bits & (1 << static_cast<u8>(bit_usage::mod))) {
            if (mod == 0b11) {
                *mod_operand = get_reg_operand(rm, (w || (bits[bits_at(bit_usage::rm_reg_always_w)])));
            } else {
                mod_operand->type = operand_type::memory;
                mod_operand->address.segment = ctx->default_segment;
                mod_operand->address.displacement = displacement;

                if ((mod == 0b00) && rm == 0b110) {
                    mod_operand->address.base = effective_addr_base::direct;
                } else {
                    mod_operand->address.base = static_cast<effective_addr_base>(1 + rm);
                }
            }

        }

        //
        // NOTE(casey): Because there are some strange opcodes that do things like have an immediate as
        // a _destination_ ("out", for example), I define immediates and other "additional operands" to
        // go in "whatever slot was not used by the reg and mod fields".
        //
        instr_operand *last_operand = &dest.operands[0];
        if (last_operand->type != operand_type::none) {
            last_operand = &dest.operands[1];
        }

        if (bits_at(bit_usage::rel_jmpd_disp)) {
            last_operand->type = operand_type::relative_immediate;
            last_operand->immediate_i32 = displacement + dest.size;
        }

        if (bits_at(bit_usage::has_data)) {
            last_operand->type = operand_type::immediate;
            last_operand->immediate_u32 = bits_at(bit_usage::data);
        }

        if (has_bits & (1 << static_cast<u8>(bit_usage::v))) {
            if (bits_at(bit_usage::v)) {
                last_operand->type = operand_type::reg;
                last_operand->reg.index = register_index::c;
                last_operand->reg.offset = 0;
                last_operand->reg.count = 1;
            } else {
                last_operand->type = operand_type::immediate;
                last_operand->immediate_i32 = 1;
            }
        }

    }

    return dest;
}

instr decode_instruction(disasm_context* ctx, memory* mem, segmented_access* at)
{
    instr result = {};
    for (u32 i = 0; i < ARRAY_COUNT(instr_formats); ++i) {
        instr_format fmt = instr_formats[i];
        result = try_decode(ctx, &fmt, mem, *at);

        if (static_cast<i32>(result.op)) {
            std::cerr << "failed to decode instruction" << std::endl;
            break;
        }
    }

    return result;
}

// TODO(lm): use switch
void update_context(disasm_context* ctx, instr instr)
{
    if (instr.op == operation_type::op_lock) {
        ctx->additional_flags |= static_cast<u32>(instr_flag::lock);
    } else if (instr.op == operation_type::op_rep) {
        ctx->additional_flags |= static_cast<u32>(instr_flag::rep);
    } else if (instr.op == operation_type::op_segment) {
        ctx->additional_flags |= static_cast<u32>(instr_flag::segment);
    } else {
        ctx->additional_flags = 0;
        ctx->default_segment = register_index::ds;
    }
}

}
