#include "common.h"
#include <cstdio>

namespace h86 {

char const* opcode_mnemonics[] = {
#define INST(mnemonic, ...) #mnemonic
#define INSTALT(...)
#include "instr_table.inl"
};

static char const* get_mnemonic(operation_type op)
{
    return opcode_mnemonics[static_cast<usize>(op)];
}

static char const* get_reg_name(register_access reg)
{
    char const* names[][3] = {
        {"", "", ""},
        {"al", "ah", "ax"},
        {"bl", "bh", "bx"},
        {"cl", "ch", "cx"},
        {"dl", "dh", "dx"},
        {"sp", "sp", "sp"},
        {"bp", "bp", "bp"},
        {"si", "si", "si"},
        {"di", "di", "di"},
        {"es", "es", "es"},
        {"cs", "cs", "cs"},
        {"ss", "ss", "ss"},
        {"ds", "ds", "ds"},
        {"ip", "ip", "ip"},
        {"flags", "flags", "flags"}
    };
    static_assert(ARRAY_COUNT(names) == static_cast<usize>(register_index::_count));

    return names[static_cast<usize>(reg.index)][reg.count];
}

static char const* get_effective_address_expr(effective_addr_expr expr)
{
    char const* rm_base[] = {
        "",
        "bx+si",
        "bx+di",
        "bp+si",
        "bp+di",
        "si",
        "di",
        "bp",
        "bx",
    };
    static_assert(ARRAY_COUNT(rm_base) == static_cast<usize>(effective_addr_base::_count));

    return rm_base[static_cast<usize>(expr.base)];
}

b32 is_printable(instr instr)
{
    return !((instr.op == operation_type::op_lock) ||
             (instr.op == operation_type::op_rep) ||
             (instr.op == operation_type::op_segment));
}

void print_instr(instr instr, FILE *dest)
{
    u32 flags = instr.flags;
    u32 w = flags & static_cast<u8>(instr_flag::wide);

    if (flags & static_cast<u8>(instr_flag::lock)) {
        if (instr.op == operation_type::op_xchg) {
            // NOTE(casey): This is just a stupidity for matching assembler expectations.
            instr_operand tmp = instr.operands[0];
            instr.operands[0] = instr.operands[1];
            instr.operands[1] = tmp;
        }
        fprintf(dest, "lock ");
    }

    const char* mnemonic_suffix = "";
    if (flags & static_cast<u32>(instr_flag::rep)) {
        // TODO(lm): check, no fprintf
        printf("rep ");
        mnemonic_suffix = w ? "w" : "b";
    }

    fprintf(dest, "%s%s", get_mnemonic(instr.op), mnemonic_suffix);

    const char* separator = "";
    for (u32 i = 0; i < ARRAY_COUNT(instr.operands); ++i) {
        instr_operand operand = instr.operands[i];

        if (operand.type != operand_type::none) {
            fprintf(dest, "%s", separator);
            separator = ", ";

            switch (operand.type) {
                case operand_type::none:
                    break;

                case operand_type::reg: {
                    fprintf(dest, "%s", get_reg_name(operand.reg));
                } break;

                case operand_type::memory: {
                    effective_addr_expr addr = operand.address;
                    if (instr.operands[0].type != operand_type::reg) {
                        fprintf(dest, "%s ", w ? "word" : "byte");
                    }

                    if (flags & static_cast<u32>(instr_flag::segment)) {
                        printf("%s:", get_reg_name({addr.segment, 0, 2}));
                    }

                    fprintf(dest, "[%s", get_effective_address_expr(addr));
                    if (addr.displacement != 0) {
                        fprintf(dest, "%+d", addr.displacement);
                    }
                    fprintf(dest, "]");
                } break;

                case operand_type::immediate: {
                    fprintf(dest, "%d", operand.immediate_i32);
                } break;

                case operand_type::relative_immediate: {
                    fprintf(dest, "$%+d", operand.immediate_i32);
                } break;
            }
        }
    }
}

}
