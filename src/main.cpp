#include <cstdio>
#include <iostream>

#include "common.h"
#include "memory.h"
#include "decode.h"
#include "text.h"

namespace h86 {

static void disasm(memory* mem, u32 disasm_byte_count, segmented_access disasm_start)
{
    segmented_access at = disasm_start;
    disasm_context ctx = default_disasm_context();

    u32 count = disasm_byte_count;
    while (count) {
        instr instr = decode_instruction(&ctx, mem, &at);


        if (instr.op == operation_type::op_none) {
            fprintf(stderr, "ERROR: Unrecognized binary in instruction stream.\n");
        }

        if (count >= instr.size) {
            count -= instr.size;
        } else {
            fprintf(stderr, "ERROR: Instruction extends outside disassembly region\n");
            break;
        }

        update_context(&ctx, instr);
        if (is_printable(instr)) {
            print_instr(instr, stdout);
            printf("\n");
        }
    }
}

}

int main(int argc, char** argv)
{
    h86::memory* mem = (h86::memory*)malloc(sizeof(h86::memory));
    if (argc == 1) {
        fprintf(stderr, "USAGE: %s [8086 machine code file] ...\n", argv[0]);
    }

    for (int i = 1; i < argc; ++i) {
        char* file_name = argv[i];
        u32 bytes_read = load_memory_from_file(file_name, mem, 0);

        printf("; %s disassembly:\n", file_name);
        printf("bits 16\n");
        h86::disasm(mem, bytes_read, {});
    }

    return 0;
}

