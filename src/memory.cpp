#include "memory.h"
#include <cassert>
#include <cstdio>

namespace h86 {

u32 absolute_address_of(u16 base, u16 offset, u16 additional)
{
    return (((u32)base << 4) + (u32)(offset + additional)) & MEMORY_ACCESS_MASK;
}

u32 absolute_address_of(segmented_access access, u16 additional)
{
    return absolute_address_of(access.segment_base, access.segment_offset, additional);
}

u8  read_memory(memory* memory, u32 absolute_address)
{
    assert(absolute_address < ARRAY_COUNT(memory->bytes));
    return memory->bytes[absolute_address];
}

u32 load_memory_from_file(char* file_name, memory* memory, u32 at_offset)
{
    u32 result = 0;

    if (at_offset >= ARRAY_COUNT(memory->bytes)) {
        return result;
    }

    FILE* file = fopen(file_name, "rb");
    if (!file) {
        result = fread(memory->bytes + at_offset, 1, ARRAY_COUNT(memory->bytes), file);
        fclose(file);
    } else {
        fprintf(stderr, "ERROR: Unable to open %s.\n", file_name);
    }

    return result;
}

}

