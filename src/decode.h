#include "common.h"
#include "memory.h"

namespace h86 {

// ----------------------------------------------

enum class bit_usage : u8  {
    literal,
    mod,
    reg,
    rm,
    sr,
    disp,
    data,

    has_disp,
    disp_always_w,
    has_data,
    w_makes_data_w,
    rm_reg_always_w,
    rel_jmpd_disp,

    d,
    s,
    w,
    v,
    z,

    _count
};

// ----------------------------------------------

struct instr_bits {
    bit_usage usage;
    u8              bit_count;
    u8              shift;
    u8              value;
};

// ----------------------------------------------

struct instr_format {
    operation_type  op;
    instr_bits      bits[16];
};

// ----------------------------------------------

struct disasm_context {
    register_index  default_segment;
    u32             additional_flags;
};

// ----------------------------------------------

disasm_context default_disasm_context();
instr decode_instruction(disasm_context* ctx, memory* mem, segmented_access* at);
void update_context(disasm_context* ctx, instr instr);

}
