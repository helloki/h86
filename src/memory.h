#pragma once

#include "common.h"

namespace h86 {

// ----------------------------------------------

struct memory {
    u8 bytes[1024*1024];
};

#define MEMORY_ACCESS_MASK 0xfffff
// static_assert((ARRAY_COUNT(Memory::bytes), -1) == MEMORY_ACCESS_MASK,
//              "Memory size doesn't match access-mask");

// ----------------------------------------------

struct segmented_access {
    u16 segment_base;
    u16 segment_offset;
};

// ----------------------------------------------

u32 absolute_address_of(u16 base, u16 offset, u16 additional = 0);
u32 absolute_address_of(segmented_access access, u16 additional = 0);
u8  read_memory(memory* memory, u32 absolute_address);
u32 load_memory_from_file(char* file_name, memory* memory, u32 at_offset);

// ----------------------------------------------

}

