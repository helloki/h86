#include <cstdio>
#include "common.h"

namespace h86 {

b32 is_printable(instr instr);
void print_instr(instr instr, FILE* dest);

}
